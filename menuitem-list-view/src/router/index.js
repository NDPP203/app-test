import React from 'react';
import { createNativeStackNavigator, } from '@react-navigation/native-stack';
import ListView from '../listView';
import TreeView from '../treeView';
import Detail from '../listView/detail';
import Add from '../listView/add';
import HelloDemo from '../Hello/demo';
import Apis from '../Hello/apii'
import Details from '../Hello/detais';
import loginn from '../login/login';
import pickSchools from '../pickSchool/index'
const Stack = createNativeStackNavigator();

const Router = () => {
  return (
    <Stack.Navigator
      initialRouteName='pickSchools'
    >
      <Stack.Screen
        name="ListView"
        component={ListView}
        options={{ title: 'ListView' }}
      />
      <Stack.Screen
        name="TreeView"
        component={TreeView}
        options={{ title: 'TreeView' }}
      />
      <Stack.Screen
        name="Detail"
        component={Detail}
        options={{ title: 'Detail' }}
      />
      <Stack.Screen
        name="Apis"
        component={Apis}
        options={{ title: 'Apis' }}
      />
        <Stack.Screen
         name="Details"
         component={Details}
         options={{ title: 'Details' }}
       />
      <Stack.Screen
        name="HelloDemo"
        component={HelloDemo}
        options={{ title: 'helo' }}
        
      />
      <Stack.Screen
        name="loginn"
        component={loginn}
        options={{ title: 'loginn'}}
      />
      <Stack.Screen
      name='pickSchools'
      component={pickSchools}
      options={{title: 'pickSchools'}}
      />
    </Stack.Navigator>
  );
};
export default Router