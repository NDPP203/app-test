/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  TouchableOpacity,
  Picker,
  TextInput,
  Button,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import RadioButton from '../Components/radioButton';
import Utils from '../utils/Utils';

const Section = ({children, title}): Node => {
  const isDarkMode = useColorScheme() === 'dark';
  return (
    <View style={styles.sectionContainer}>
      <Text
        style={[
          styles.sectionTitle,
          {
            color: isDarkMode ? Colors.white : Colors.black,
          },
        ]}>
        {title}
      </Text>
      <Text
        style={[
          styles.sectionDescription,
          {
            color: isDarkMode ? Colors.light : Colors.dark,
          },
        ]}>
        {children}
      </Text>
    </View>
  );
};
const itemSelectedIds = [];
const item_list = false;
const filter2 = [];
const status = [];
const statusArray = [
  {id: 1, label: 'Chủ trì', children: []},
  {
    id: 2,
    label: 'Phối hợp',
    children: [],
  },
  {
    id: 3,
    label: 'Thông báo',
    children: [],
  },
];

const TreeView = () => {
  const [dataStatus, setStatus] = useState(statusArray);
  const [searchItem, setSearchItem] = useState(itemSelectedIds);
  const [data, setData] = useState(dataTree);
  const isDarkMode = useColorScheme() === 'dark';
  const [fresh, setFresh] = useState(false);
  const [set_item_list, setItemList] = useState(item_list);
  const [set_Filter2, setFilter2] = useState(filter2);
  const [txtSearch, setTxtSearch] = useState('');
  const backgroundStyle = {
    // backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  const toggleNode = (
    index,
    item,
    children_all,
    children_leader,
    children_special,
    children_room,
  ) => {
    if (children_all != null) {
      children_all.check = !children_all.check;
    }
    if (item != null) {
      item.check = !item.check;
    }
    if (index != null) {
      index = !index;
    }
    if (children_leader != null) {
      children_leader.check = !children_leader.check;
    }
    if (children_special != null) {
      children_special.check = !children_special.check;
    }

    setFresh(!fresh);
  };

  const onPressLV3 = (
    item,
    father_level_1,
    father_level_2,
    father_level_3,
    father_level_4,
  ) => {
    if (itemSelectedIds.indexOf(item) >= 0) {
      itemSelectedIds.splice(itemSelectedIds.indexOf(item), 1);
      if (item.children.length > 0) {
        item.children.forEach(function element1(element) {
          itemSelectedIds.splice(itemSelectedIds.indexOf(element), 1);

          if (element.children.length > 0) {
            element.children.forEach(function element2(element2) {
              itemSelectedIds.splice(itemSelectedIds.indexOf(element2), 1);
              if (element2.children.length > 0) {
                element2.children.forEach(function element3(element3) {
                  itemSelectedIds.splice(itemSelectedIds.indexOf(element3), 1);

                  if (element3.children.length > 0) {
                    element3.children.forEach(function element3(element4) {
                      itemSelectedIds.splice(
                        itemSelectedIds.indexOf(element4),
                        1,
                      );
                    });
                  }
                  // itemSelectedIds.splice(itemSelectedIds.indexOf(element3), 1);
                });
              }
              // itemSelectedIds.splice(itemSelectedIds.indexOf(element2), 1);
            });
          }
          // itemSelectedIds.splice(itemSelectedIds.indexOf(element), 1);
        });
      }
    } else {
      itemSelectedIds.push(item);

      if (item.children.length > 0) {
        item.children.forEach(function element1(element) {
          if (itemSelectedIds.indexOf(element) >= 0) {
            itemSelectedIds.splice(itemSelectedIds.indexOf(element), 1);
          }
          itemSelectedIds.push(element);
          if (element.children.length > 0) {
            element.children.forEach(function element2(element2) {
              if (itemSelectedIds.indexOf(element2) >= 0) {
                itemSelectedIds.splice(itemSelectedIds.indexOf(element2), 1);
              }

              itemSelectedIds.push(element2);
              if (element2.children.length > 0) {
                element2.children.forEach(function element3(element3) {
                  if (itemSelectedIds.indexOf(element3) >= 0) {
                    itemSelectedIds.splice(
                      itemSelectedIds.indexOf(element3),
                      1,
                    );
                  }
                  itemSelectedIds.push(element3);
                  if (element3.children.length > 0) {
                    element3.children.forEach(function element3(element4) {
                      if (itemSelectedIds.indexOf(element4) >= 0) {
                        itemSelectedIds.splice(
                          itemSelectedIds.indexOf(element4),
                          1,
                        );
                      }
                      itemSelectedIds.push(element4);
                    });
                  }
                  // itemSelectedIds.push(element3);
                });
              }
              // itemSelectedIds.push(element2);
            });
          }
          // itemSelectedIds.push(element);
        });
      }
    }

    check_all(
      item,
      father_level_1,
      father_level_2,
      father_level_3,
      father_level_4,
    );
    selectStatus(4, item);
    setFresh(!fresh);
  };

  function check_all(
    item,
    father_level_1,
    father_level_2,
    father_level_3,
    father_level_4,
  ) {
    if (father_level_1 == null) {
      father_level_1 = item;
      check_all(
        item,
        father_level_1,
        father_level_2,
        father_level_3,
        father_level_4,
      );
    }

    const intersection = father_level_1.children.filter(element5 =>
      itemSelectedIds.includes(element5),
    );
    if (
      intersection.length == father_level_1.children.length &&
      father_level_1 != null &&
      item != father_level_1
    ) {
      itemSelectedIds.push(father_level_1);

      if (father_level_2 != null) {
        const intersection = father_level_2.children.filter(element5 =>
          itemSelectedIds.includes(element5),
        );
        if (intersection.length == father_level_2.children.length) {
          itemSelectedIds.push(father_level_2);

          if (father_level_3 != null) {
            const intersection = father_level_3.children.filter(element5 =>
              itemSelectedIds.includes(element5),
            );
            if (intersection.length == father_level_3.children.length) {
              itemSelectedIds.push(father_level_3);

              if (father_level_4 != null) {
                const intersection = father_level_4.children.filter(element5 =>
                  itemSelectedIds.includes(element5),
                );
                if (intersection.length == father_level_4.children.length) {
                  itemSelectedIds.push(father_level_4);
                }
              }
            }
          }
        }
      }
    } else if (
      itemSelectedIds.indexOf(father_level_1) >= 0 &&
      intersection.length != father_level_1.children.length
    ) {
      if (father_level_1 != null) {
        while (itemSelectedIds.indexOf(father_level_1) >= 0) {
          itemSelectedIds.splice(itemSelectedIds.indexOf(father_level_1), 1);
        }
        if (father_level_2 != null) {
          while (itemSelectedIds.indexOf(father_level_2) >= 0) {
            itemSelectedIds.splice(itemSelectedIds.indexOf(father_level_2), 1);
          }
          if (father_level_3 != null) {
            while (itemSelectedIds.indexOf(father_level_3) >= 0) {
              itemSelectedIds.splice(
                itemSelectedIds.indexOf(father_level_3),
                1,
              );
            }
            if (father_level_4 != null) {
              while (itemSelectedIds.indexOf(father_level_4) >= 0) {
                itemSelectedIds.splice(
                  itemSelectedIds.indexOf(father_level_4),
                  1,
                );
              }
            }
          }
        }
      }
    }
  }
  const deleteItem = item_data => {
    let father_level_1 = null;
    let father_level_2 = null;
    let father_level_3 = null;
    let father_level_4 = null;
    itemSelectedIds.map(function (father) {
      if (father.children.length > 0) {
        if (
          father.children
            .map(function (e) {
              return e;
            })
            .indexOf(item_data) >= 0
        ) {
          father_level_1 = father;

          if (father_level_1 != null) {
            itemSelectedIds.map(function (father) {
              if (father.children.length > 0) {
                if (
                  father.children
                    .map(function (e) {
                      return e;
                    })
                    .indexOf(father_level_1) >= 0
                ) {
                  father_level_2 = father;
                  if (father_level_2 != null) {
                    itemSelectedIds.map(function (father) {
                      if (father.children.length > 0) {
                        if (
                          father.children
                            .map(function (e) {
                              return e;
                            })
                            .indexOf(father_level_2) >= 0
                        ) {
                          father_level_3 = father;

                          if (father_level_3 != null) {
                            itemSelectedIds.map(function (father) {
                              if (father.children.length > 0) {
                                if (
                                  father.children
                                    .map(function (e) {
                                      return e;
                                    })
                                    .indexOf(father_level_3) >= 0
                                ) {
                                  father_level_4 = father;
                                }
                              }
                            });
                          }
                        }
                      }
                    });
                  }
                }
              }
            });
          }
        }
      }
    });

    onPressLV3(
      item_data,
      father_level_1,
      father_level_2,
      father_level_3,
      father_level_4,
    );
  };

  const renderChevron = (item1, item2, item3, item4, item5, type) => {
    let item = '';
    switch (type) {
      case 1:
        item = item1;
        break;
      case 2:
        item = item2;
        break;
      case 3:
        item = item3;
        break;
      case 4:
        item = item4;
        break;
      case 5:
        item = item5;
      default:
        break;
    }
    return (
      <TouchableOpacity
        onPress={() => toggleNode(item1, item2, item3, item4, item5)}>
        <Image
          style={{
            height: 16,
            width: 16,
            marginTop: 10,
            marginLeft: 5,
          }}
          source={
            item.check
              ? require('../asset/down-arrow.png')
              : require('../asset/up-arrow.png')
          }
        />
      </TouchableOpacity>
    );
  };

  const renderStatus = obj => {
    return (
      <View
        style={{flexDirection: 'row', flex: 1, justifyContent: 'flex-start'}}>
        <TouchableOpacity
          style={{flexDirection: 'row',marginRight: 8}}
          onPress={() => selectStatus(Utils.TYPE_RADIO.CHU_TRI, obj)}>
          <Image
            style={{height: 20, width: 20}}
            source={
              dataStatus[0].children.indexOf(obj) >= 0
                ? require('../asset/radio-button-select.png')
                : require('../asset/radio-button.png')
            }
          />
          <Text style={{fontSize:11}}>Chủ trì</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={{flexDirection: 'row',marginRight: 8}}
          onPress={() => selectStatus(Utils.TYPE_RADIO.PHOI_HOP, obj)}>
          <Image
            style={{height: 20, width: 20}}
            source={
              dataStatus[1].children.indexOf(obj) >= 0
                ? require('../asset/radio-button-select.png')
                : require('../asset/radio-button.png')
            }
          />
          <Text style={{fontSize:11}}>Phối hợp</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={{flexDirection: 'row',marginRight: 8}}
          onPress={() => selectStatus(Utils.TYPE_RADIO.THONG_BAO, obj)}>
          <Image
            style={{height: 20, width: 20}}
            source={
              dataStatus[2].children.indexOf(obj) >= 0
                ? require('../asset/radio-button-select.png')
                : require('../asset/radio-button.png')
            }
          />
          <Text style={{fontSize:11}}>Thông báo</Text>
        </TouchableOpacity>
      </View>
    );
  };
  const selectStatus = (id, index) => {
    console.log(dataStatus);
    switch (id) {
      case 0:
        if (dataStatus[1].children.indexOf(index) >= 0) {
          dataStatus[1].children.splice(
            dataStatus[1].children.indexOf(index),
            1,
          );
        }
        if (dataStatus[2].children.indexOf(index) >= 0) {
          dataStatus[2].children.splice(
            dataStatus[2].children.indexOf(index),
            1,
          );
        }
        if (dataStatus[0].children.indexOf(index) >= 0) {
          dataStatus[0].children.splice(
            dataStatus[0].children.indexOf(index),
            1,
          );
        } else if (dataStatus[id].children.indexOf(index) < 0) {
          if (dataStatus[0].children.length <= 0) {
          } else {
            dataStatus[1].children.push(dataStatus[0].children[0]);
          }
          dataStatus[id].children = [];
          dataStatus[id].children.push(index);
        }

        setFresh(!fresh);

        break;
      case 1:
        if (dataStatus[0].children.indexOf(index) >= 0) {
          dataStatus[0].children.splice(
            dataStatus[0].children.indexOf(index),
            1,
          );
        }

        if (dataStatus[2].children.indexOf(index) >= 0) {
          dataStatus[2].children.splice(
            dataStatus[2].children.indexOf(index),
            1,
          );
        }
        if (dataStatus[1].children.indexOf(index) >= 0) {
          dataStatus[1].children.splice(
            dataStatus[1].children.indexOf(index),
            1,
          );
        } else if (dataStatus[id].children.indexOf(index) < 0) {
          dataStatus[id].children.push(index);
        }
        setFresh(!fresh);
        break;
      case 2:
        if (dataStatus[0].children.indexOf(index) >= 0) {
          dataStatus[0].children.splice(
            dataStatus[0].children.indexOf(index),
            1,
          );
        }
        if (dataStatus[1].children.indexOf(index) >= 0) {
          dataStatus[1].children.splice(
            dataStatus[1].children.indexOf(index),
            1,
          );
        }
        if (dataStatus[2].children.indexOf(index) >= 0) {
          dataStatus[2].children.splice(
            dataStatus[2].children.indexOf(index),
            1,
          );
        } else if (dataStatus[id].children.indexOf(index) < 0) {
          dataStatus[id].children.push(index);
        }
        setFresh(!fresh);
        break;

      case 4:
        dataStatus[0].children.forEach(item => {
          if (itemSelectedIds.indexOf(item) < 0) {
            dataStatus[0].children.splice(
              dataStatus[0].children.indexOf(item),
              1,
            );
          }
        });
        dataStatus[1].children = [];

        dataStatus[2].children.forEach(item => {
          if (itemSelectedIds.indexOf(item) < 0) {
            dataStatus[2].children.splice(
              dataStatus[2].children.indexOf(item),
              1,
            );
          }
        });

        itemSelectedIds.forEach(item => {
          pushDataSample(item);
        });
        function pushDataSample(item) {
          if (
            dataStatus[2].children.indexOf(item) >= 0 ||
            dataStatus[0].children.indexOf(item) >= 0
          ) {
          } else dataStatus[1].children.push(item);
        }
        console.log(dataStatus);
        setFresh(!fresh);
      default:
        break;
    }
  };
  const renderMenu = (
    childrenMenu,
    item,
    children_all,
    children_leader,
    children_special,
    children_room,
  ) => {
    return (
      <TouchableOpacity
        style={{
          flexDirection: 'row',
        }}
        onPress={() =>
          onPressLV3(
            item,
            children_all,
            children_leader,
            children_special,
            children_room,
          )
        }>
        <Image
          style={{height: 20, width: 20}}
          source={
            itemSelectedIds.indexOf(childrenMenu) >= 0
              ? require('../asset/radio_on.png')
              : require('../asset/radio_off.png')
          }
        />
        <Text>{childrenMenu.label}</Text>
      </TouchableOpacity>
    );
  };
  function displayList(item_list) {
    item_list = !item_list;
    setItemList(item_list);
    setSearchItem(itemSelectedIds);
  }

  const onChangeText = txt => {
    setTxtSearch(txt);
    let filter = itemSelectedIds.filter(function (item) {
      return item.label.includes(txt);
    });
    setSearchItem(filter);
    onChangTextTow(txt);
  };

  const onChangTextTow = txt => {
    const dataFatten = [];
    data.forEach(item => {
      dataFatten.push(item);
      if (item.children.length > 0) {
        item.children.forEach(item2 => {
          dataFatten.push(item2);
          if (item2.children.length > 0) {
            item2.children.forEach(item3 => {
              dataFatten.push(item3);
              if (item3.children.length > 0) {
                item3.children.forEach(item4 => {
                  dataFatten.push(item4);
                  if (item4.children.length > 0) {
                    item4.children.forEach(item5 => {
                      dataFatten.push(item5);
                    });
                  }
                });
              }
            });
          }
        });
      }
    });

    setTxtSearch(txt);
    const filter2 = dataFatten.filter(function (item) {
      return item.label.includes(txt);
    });
    setFilter2(filter2);
  };

  return (
    <SafeAreaView
    // style={backgroundStyle}
    >
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        // style={backgroundStyle}
      >
        <View>
          <TextInput
            // onChangeText={value => this.setState({search: value})}
            onChangeText={onChangeText}
            value={txtSearch}
          />
          {/* <Button title="click me" onPress={this.search} /> */}
        </View>
        <View
          style={{
            backgroundColor: isDarkMode ? Colors.black : Colors.white,
            padding: 16,
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
            flexWrap: 'wrap',
          }}>
          {set_item_list ? (
            searchItem.map(item_data => (
              <TouchableOpacity
                style={{flexDirection: 'row'}}
                onPress={() => deleteItem(item_data)}>
                <Image
                  style={{height: 20, width: 20}}
                  source={require('../asset/close.png')}
                />
                <Text>{item_data.label}</Text>
              </TouchableOpacity>
            ))
          ) : (
            <></>
          )}

          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={{width: 100}}>{'TREE VIEW'}</Text>
            <TouchableOpacity onPress={() => displayList(set_item_list)}>
              <Image
                style={{marginLeft: 150}}
                source={require('../asset/list.png')}></Image>
            </TouchableOpacity>
          </View>
          {data.map((item, index) => (
            <View style={{flex: 1}}>
              {set_Filter2 == '' || set_Filter2.indexOf(item) >= 0 ? (
                <View
                  style={{
                    flexDirection: 'row',
                    marginBottom: 5,
                  }}>
                  {renderMenu(item, item, item, null, null, null)}

                  {renderChevron(index, item, null, null, null, 2)}
                </View>
              ) : (
                <></>
              )}

              {!item.check ? (
                item.children.map((children_all, index) => (
                  <View style={{flex: 1}}>
                    {set_Filter2 == '' ||
                    set_Filter2.indexOf(children_all) >= 0 ? (
                      <View>
                        <View
                          style={{
                            marginBottom: 5,
                            flexDirection: 'row',
                          }}>
                          {renderMenu(
                            children_all,
                            children_all,
                            item,
                            null,
                            null,
                            null,
                            null,
                          )}

                          {children_all.children.length > 0 ? (
                            renderChevron(
                              null,
                              null,
                              children_all,
                              null,
                              null,
                              3,
                            )
                          ) : (
                            <></>
                          )}
                        </View>
                        {itemSelectedIds.indexOf(children_all) >= 0 ? (
                          /* <RadioButton
                            item={children_all}
                            selectedIds={itemSelectedIds
                            }
                          /> */
                          renderStatus(children_all)
                        ) : (
                          <></>
                        )}
                      </View>
                    ) : (
                      <></>
                    )}
                    {!children_all.check ? (
                      children_all.children.map((children_leader, index) => (
                        <View style={{flex: 1}}>
                          {set_Filter2 == '' ||
                          set_Filter2.indexOf(children_leader) >= 0 ? (
                            <View
                              style={{
                                marginLeft: 16,
                                marginBottom: 20,
                              }}>
                              <View
                                style={{
                                  flexDirection: 'row',
                                }}>
                                {renderMenu(
                                  children_leader,
                                  children_leader,
                                  children_all,
                                  item,
                                  null,
                                  null,
                                )}

                                {children_leader.children.length > 0 ? (
                                  renderChevron(
                                    null,
                                    null,
                                    null,
                                    children_leader,
                                    null,
                                    4,
                                  )
                                ) : (
                                  <></>
                                )}
                              </View>

                              {itemSelectedIds.indexOf(children_leader) >= 0 ? (
                                renderStatus(children_leader)
                              ) : (
                                <></>
                              )}
                            </View>
                          ) : (
                            <></>
                          )}
                          {!children_leader.check ? (
                            children_leader.children.map(children_special => (
                              <View style={{flex: 1}}>
                                {set_Filter2 == '' ||
                                set_Filter2.indexOf(children_special) >= 0 ? (
                                  <View
                                    style={{
                                      marginLeft: 32,
                                      marginBottom: 20,
                                    }}>
                                    <View
                                      style={{
                                        flexDirection: 'row',
                                      }}>
                                      {renderMenu(
                                        children_special,
                                        children_special,
                                        children_leader,
                                        children_all,
                                        item,
                                        null,
                                      )}

                                      {children_special.children.length > 0 ? (
                                        renderChevron(
                                          null,
                                          null,
                                          null,
                                          null,
                                          children_special,
                                          5,
                                        )
                                      ) : (
                                        <></>
                                      )}
                                    </View>
                                    {itemSelectedIds.indexOf(
                                      children_special,
                                    ) >= 0 ? (
                                      renderStatus(children_special)
                                    ) : (
                                      <></>
                                    )}
                                  </View>
                                ) : (
                                  <></>
                                )}
                                {!children_special.check ? (
                                  children_special.children.map(
                                    (children_room, index) => (
                                      <View>
                                        {set_Filter2 == '' ||
                                        set_Filter2.indexOf(children_room) >=
                                          0 ? (
                                          <View
                                            style={{
                                              marginLeft: 48,
                                              marginBottom: 10,
                                            }}>
                                            <View>
                                              {renderMenu(
                                                children_room,
                                                children_room,
                                                children_special,
                                                children_leader,
                                                children_all,
                                                item,
                                              )}
                                            </View>

                                            {itemSelectedIds.indexOf(
                                              children_room,
                                            ) >= 0 ? (
                                              renderStatus(children_room)
                                            ) : (
                                              <></>
                                            )}
                                          </View>
                                        ) : (
                                          <></>
                                        )}
                                      </View>
                                    ),
                                  )
                                ) : (
                                  <></>
                                )}
                              </View>
                            ))
                          ) : (
                            <></>
                          )}
                        </View>
                      ))
                    ) : (
                      <></>
                    )}
                  </View>
                ))
              ) : (
                <></>
              )}
            </View>
          ))}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default TreeView;

const dataTree = [
  {
    check: false,
    id: 1,
    key: '1',
    label: 'Tất cả',
    isWorkqueue: false,
    eId: 0,
    deptId: 0,
    nodeType: -1,
    children: [
      {
        check: false,
        id: 2,
        key: '2',
        label: 'Lãnh đạo Đơn vị',
        isWorkqueue: false,
        parentid: 1,
        eId: 0,
        deptId: 0,
        nodeType: -1,
        children: [
          {
            check: false,
            id: 3,
            key: '3',
            userId: 11,
            label: 'Cục Tin học và Thống kê tài chính',
            code: 'THTK',
            orgCode: 'THTK',
            activityId: 2,
            isWorkqueue: false,
            parentid: 2,
            eId: 11,
            deptName: 'Cục Tin học và Thống kê tài chính',
            deptId: 9,
            nodeType: -1,
            children: [
              {
                check: false,
                id: 4,
                key: '4',
                userId: 1022,
                label: 'Nguyễn Đại Trí',
                code: 'THTK',
                orgCode: 'THTK',
                activityId: 2,
                isWorkqueue: false,
                parentid: 3,
                eId: 11,
                roleName: 'Lãnh đạo đơn vị',
                deptName: 'Cục Tin học và Thống kê tài chính',
                deptId: 9,
                nodeType: 0,
                children: [],
              },
              {
                check: false,
                id: 5,
                key: '5',
                userId: 1004,
                label: 'Hoàng Xuân Nam',
                code: 'THTK',
                orgCode: 'THTK',
                activityId: 2,
                isWorkqueue: false,
                parentid: 3,
                eId: 12,
                roleName: 'Phó lãnh đạo đơn vị',
                deptName: 'Cục Tin học và Thống kê tài chính',
                deptId: 9,
                nodeType: 0,
                children: [],
              },
            ],
          },

          {
            check: false,
            id: 9,
            key: '9',
            label: 'Lãnh đạo Bộ phận',
            isWorkqueue: false,
            parentid: 1,
            eId: 0,
            deptId: 0,
            nodeType: -1,
            children: [
              {
                check: false,
                id: 10,
                key: '10',
                userId: 16,
                label: 'Phòng Thống kê',
                code: 'TK',
                orgCode: 'THTK',
                activityId: 3,
                isWorkqueue: false,
                parentid: 9,
                eId: 16,
                roleName: 'Lãnh đạo phòng',
                deptName: 'Phòng Thống kê',
                deptId: 54,
                nodeType: 2,
                children: [
                  {
                    check: false,
                    id: 11,
                    key: '11',
                    userId: 985,
                    label: 'Bùi Tiến Sỹ',
                    code: 'TK',
                    orgCode: 'THTK',
                    activityId: 3,
                    isWorkqueue: false,
                    parentid: 10,
                    eId: 16,
                    roleName: 'Lãnh đạo phòng',
                    deptName: 'Phòng Thống kê',
                    deptId: 54,
                    nodeType: 0,
                    children: [],
                  },
                  {
                    check: false,
                    id: 12,
                    key: '12',
                    userId: 1066,
                    label: 'Phạm Thanh Huyền',
                    code: 'TK',
                    orgCode: 'THTK',
                    activityId: 3,
                    isWorkqueue: false,
                    parentid: 10,
                    eId: 17,
                    roleName: 'Phó lãnh đạo phòng',
                    deptName: 'Phòng Thống kê',
                    deptId: 54,
                    nodeType: 0,
                    children: [],
                  },
                  {
                    check: false,
                    id: 13,
                    key: '13',
                    userId: 1051,
                    label: 'Nguyễn Thu Hà',
                    code: 'TK',
                    orgCode: 'THTK',
                    activityId: 3,
                    isWorkqueue: false,
                    parentid: 10,
                    eId: 17,
                    roleName: 'Phó lãnh đạo phòng',
                    deptName: 'Phòng Thống kê',
                    deptId: 54,
                    nodeType: 0,
                    children: [],
                  },
                ],
              },
              {
                check: false,
                id: 14,
                key: '14',
                userId: 16,
                label: 'Phòng Thẩm định và kiểm tra',
                code: 'TĐKT',
                inputCode: '04',
                orgCode: 'THTK',
                activityId: 3,
                isWorkqueue: false,
                parentid: 9,
                eId: 16,
                roleName: 'Lãnh đạo phòng',
                deptName: 'Phòng Thẩm định và kiểm tra',
                deptId: 85,
                nodeType: 2,
                children: [
                  {
                    check: false,
                    id: 15,
                    key: '15',
                    userId: 990,
                    label: 'Đào Lê Hương Giang',
                    code: 'TĐKT',
                    orgCode: 'THTK',
                    activityId: 3,
                    isWorkqueue: false,
                    parentid: 10,
                    eId: 16,
                    roleName: 'Lãnh đạo phòng',
                    deptName: 'Phòng Thẩm định và kiểm tra',
                    deptId: 85,
                    nodeType: 0,
                    children: [],
                  },
                  {
                    check: false,
                    id: 16,
                    key: '16',
                    userId: 1084,
                    label: 'Trần Thị Xuân Phượng',
                    code: 'TĐKT',
                    orgCode: 'THTK',
                    activityId: 3,
                    isWorkqueue: false,
                    parentid: 10,
                    eId: 17,
                    roleName: 'Phó lãnh đạo phòng',
                    deptName: 'Phòng Thẩm định và kiểm tra',
                    deptId: 85,
                    nodeType: 0,
                    children: [],
                  },
                ],
              },
              {
                check: false,
                id: 17,
                key: '17',
                userId: 16,
                label: 'Phòng Dịch vụ công và Nội dung số',
                code: 'DVC',
                orgCode: 'THTK',
                activityId: 3,
                isWorkqueue: false,
                parentid: 9,
                eId: 16,
                roleName: 'Lãnh đạo phòng',
                deptName: 'Phòng Dịch vụ công và Nội dung số',
                deptId: 101,
                nodeType: 2,
                children: [
                  {
                    check: false,
                    id: 18,
                    key: '18',
                    userId: 1002,
                    label: 'Hoàng Thành',
                    code: 'DVC',
                    orgCode: 'THTK',
                    activityId: 3,
                    isWorkqueue: false,
                    parentid: 10,
                    eId: 16,
                    roleName: 'Lãnh đạo phòng',
                    deptName: 'Phòng Dịch vụ công và Nội dung số',
                    deptId: 101,
                    nodeType: 0,
                    children: [],
                  },
                  {
                    check: false,
                    id: 19,
                    key: '19',
                    userId: 1044,
                    label: 'Nguyễn Thị Lan Anh',
                    code: 'DVC',
                    orgCode: 'THTK',
                    activityId: 3,
                    isWorkqueue: false,
                    parentid: 10,
                    eId: 17,
                    roleName: 'Phó lãnh đạo phòng',
                    deptName: 'Phòng Dịch vụ công và Nội dung số',
                    deptId: 101,
                    nodeType: 0,
                    children: [],
                  },
                ],
              },
              {
                check: false,
                id: 20,
                key: '20',
                userId: 16,
                label: 'Phòng Quản lý an toàn thông tin',
                code: 'ANTT',
                orgCode: 'THTK',
                activityId: 3,
                isWorkqueue: false,
                parentid: 9,
                eId: 16,
                roleName: 'Lãnh đạo phòng',
                deptName: 'Phòng Quản lý an toàn thông tin',
                deptId: 94,
                nodeType: 2,
                children: [
                  {
                    check: false,
                    id: 21,
                    key: '21',
                    userId: 1011,
                    label: 'Lê Linh Chi',
                    code: 'ANTT',
                    orgCode: 'THTK',
                    activityId: 3,
                    isWorkqueue: false,
                    parentid: 10,
                    eId: 16,
                    roleName: 'Lãnh đạo phòng',
                    deptName: 'Phòng Quản lý an toàn thông tin',
                    deptId: 94,
                    nodeType: 0,
                    children: [],
                  },
                  {
                    check: false,
                    id: 22,
                    key: '22',
                    userId: 1082,
                    label: 'Trần Văn Trình',
                    code: 'ANTT',
                    orgCode: 'THTK',
                    activityId: 3,
                    isWorkqueue: false,
                    parentid: 10,
                    eId: 17,
                    roleName: 'Phó lãnh đạo phòng',
                    deptName: 'Phòng Quản lý an toàn thông tin',
                    deptId: 94,
                    nodeType: 0,
                    children: [],
                  },
                ],
              },
              {
                check: false,
                id: 23,
                key: '23',
                userId: 16,
                label:
                  'Trung tâm dữ liệu và triển khai công nghệ thông tin tài chính tại Thành phố HCM',
                code: 'DLTK',
                orgCode: 'THTK',
                activityId: 3,
                isWorkqueue: false,
                parentid: 9,
                eId: 16,
                roleName: 'Lãnh đạo phòng',
                deptName:
                  'Trung tâm dữ liệu và triển khai công nghệ thông tin tài chính tại Thành phố HCM',
                deptId: 114,
                nodeType: 2,
                children: [
                  {
                    check: false,
                    id: 24,
                    key: '24',
                    userId: 1023,
                    label: 'Nguyễn Đăng Lê',
                    code: 'DLTK',
                    orgCode: 'THTK',
                    activityId: 3,
                    isWorkqueue: false,
                    parentid: 10,
                    eId: 16,
                    roleName: 'Lãnh đạo phòng',
                    deptName:
                      'Trung tâm dữ liệu và triển khai công nghệ thông tin tài chính tại Thành phố HCM',
                    deptId: 114,
                    nodeType: 0,
                    children: [],
                  },
                  {
                    check: false,
                    id: 25,
                    key: '25',
                    userId: 1000,
                    label: 'Hoàng Minh Thế Nam',
                    code: 'DLTK',
                    orgCode: 'THTK',
                    activityId: 3,
                    isWorkqueue: false,
                    parentid: 10,
                    eId: 17,
                    roleName: 'Phó lãnh đạo phòng',
                    deptName:
                      'Trung tâm dữ liệu và triển khai công nghệ thông tin tài chính tại Thành phố HCM',
                    deptId: 114,
                    nodeType: 0,
                    children: [],
                  },
                  {
                    check: false,
                    id: 26,
                    key: '26',
                    userId: 1055,
                    label: 'Nguyễn Trọng Trãi',
                    code: 'DLTK',
                    orgCode: 'THTK',
                    activityId: 3,
                    isWorkqueue: false,
                    parentid: 10,
                    eId: 17,
                    roleName: 'Phó lãnh đạo phòng',
                    deptName:
                      'Trung tâm dữ liệu và triển khai công nghệ thông tin tài chính tại Thành phố HCM',
                    deptId: 114,
                    nodeType: 0,
                    children: [],
                  },
                ],
              },
              {
                check: false,
                id: 27,
                key: '27',
                userId: 16,
                label: 'Phòng Quản lý Công nghệ thông tin',
                code: 'CNTT',
                orgCode: 'THTK',
                activityId: 3,
                isWorkqueue: false,
                parentid: 9,
                eId: 16,
                roleName: 'Lãnh đạo phòng',
                deptName: 'Phòng Quản lý Công nghệ thông tin',
                deptId: 33,
                nodeType: 2,
                children: [
                  {
                    check: false,
                    id: 28,
                    key: '28',
                    userId: 1027,
                    label: 'Nguyễn Hồng Đoàn',
                    code: 'CNTT',
                    orgCode: 'THTK',
                    activityId: 3,
                    isWorkqueue: false,
                    parentid: 10,
                    eId: 16,
                    roleName: 'Lãnh đạo phòng',
                    deptName: 'Phòng Quản lý Công nghệ thông tin',
                    deptId: 33,
                    nodeType: 0,
                    children: [],
                  },
                  {
                    check: false,
                    id: 29,
                    key: '29',
                    userId: 1028,
                    label: 'Nguyễn Hồng Hà',
                    code: 'CNTT',
                    orgCode: 'THTK',
                    activityId: 3,
                    isWorkqueue: false,
                    parentid: 10,
                    eId: 17,
                    roleName: 'Phó lãnh đạo phòng',
                    deptName: 'Phòng Quản lý Công nghệ thông tin',
                    deptId: 33,
                    nodeType: 0,
                    children: [],
                  },
                  {
                    check: false,
                    id: 30,
                    key: '30',
                    userId: 1071,
                    label: 'Phan Minh Duy',
                    code: 'CNTT',
                    orgCode: 'THTK',
                    activityId: 3,
                    isWorkqueue: false,
                    parentid: 10,
                    eId: 17,
                    roleName: 'Phó lãnh đạo phòng',
                    deptName: 'Phòng Quản lý Công nghệ thông tin',
                    deptId: 33,
                    nodeType: 0,
                    children: [],
                  },
                ],
              },
              {
                check: false,
                id: 31,
                key: '31',
                userId: 16,
                label: 'Phòng Quản lý hạ tầng kỹ thuật',
                code: 'HTKT',
                orgCode: 'THTK',
                activityId: 3,
                isWorkqueue: false,
                parentid: 9,
                eId: 16,
                roleName: 'Lãnh đạo phòng',
                deptName: 'Phòng Quản lý hạ tầng kỹ thuật',
                deptId: 110,
                nodeType: 2,
                children: [
                  {
                    check: false,
                    id: 32,
                    key: '32',
                    userId: 1063,
                    label: 'Nguyễn Xuân Cường',
                    code: 'HTKT',
                    orgCode: 'THTK',
                    activityId: 3,
                    isWorkqueue: false,
                    parentid: 10,
                    eId: 16,
                    roleName: 'Lãnh đạo phòng',
                    deptName: 'Phòng Quản lý hạ tầng kỹ thuật',
                    deptId: 110,
                    nodeType: 0,
                    children: [],
                  },
                  {
                    check: false,
                    id: 33,
                    key: '33',
                    userId: 1039,
                    label: 'Nguyễn Quý Bách',
                    code: 'HTKT',
                    orgCode: 'THTK',
                    activityId: 3,
                    isWorkqueue: false,
                    parentid: 10,
                    eId: 17,
                    roleName: 'Phó lãnh đạo phòng',
                    deptName: 'Phòng Quản lý hạ tầng kỹ thuật',
                    deptId: 110,
                    nodeType: 0,
                    children: [],
                  },
                  {
                    check: false,
                    id: 34,
                    key: '34',
                    userId: 1073,
                    label: 'Phùng Phương Nam',
                    code: 'HTKT',
                    orgCode: 'THTK',
                    activityId: 3,
                    isWorkqueue: false,
                    parentid: 10,
                    eId: 17,
                    roleName: 'Phó lãnh đạo phòng',
                    deptName: 'Phòng Quản lý hạ tầng kỹ thuật',
                    deptId: 110,
                    nodeType: 0,
                    children: [],
                  },
                ],
              },
              {
                check: false,
                id: 35,
                key: '35',
                userId: 16,
                label: 'Phòng Kế hoạch tổng hợp',
                code: 'KHTH',
                orgCode: 'THTK',
                activityId: 3,
                isWorkqueue: false,
                parentid: 9,
                eId: 16,
                roleName: 'Lãnh đạo phòng',
                deptName: 'Phòng Kế hoạch tổng hợp',
                deptId: 45,
                nodeType: 2,
                children: [
                  {
                    check: false,
                    id: 36,
                    key: '36',
                    userId: 1069,
                    label: 'Phạm Thượng Tình',
                    code: 'KHTH',
                    orgCode: 'THTK',
                    activityId: 3,
                    isWorkqueue: false,
                    parentid: 10,
                    eId: 16,
                    roleName: 'Lãnh đạo phòng',
                    deptName: 'Phòng Kế hoạch tổng hợp',
                    deptId: 45,
                    nodeType: 0,
                    children: [],
                  },
                  {
                    check: false,
                    id: 37,
                    key: '37',
                    userId: 1040,
                    label: 'Nguyễn Tài Quang',
                    code: 'KHTH',
                    orgCode: 'THTK',
                    activityId: 3,
                    isWorkqueue: false,
                    parentid: 10,
                    eId: 17,
                    roleName: 'Phó lãnh đạo phòng',
                    deptName: 'Phòng Kế hoạch tổng hợp',
                    deptId: 45,
                    nodeType: 0,
                    children: [],
                  },
                ],
              },
              {
                check: false,
                id: 38,
                key: '38',
                userId: 16,
                label: 'Văn phòng Cục - THTK',
                code: 'VP',
                orgCode: 'THTK',
                activityId: 3,
                isWorkqueue: false,
                parentid: 9,
                eId: 16,
                roleName: 'Lãnh đạo phòng',
                deptName: 'Văn phòng Cục - THTK',
                deptId: 62,
                nodeType: 2,
                children: [
                  {
                    check: false,
                    id: 39,
                    key: '39',
                    userId: 1080,
                    label: 'Trần Trọng Hải',
                    code: 'VP',
                    orgCode: 'THTK',
                    activityId: 3,
                    isWorkqueue: false,
                    parentid: 10,
                    eId: 16,
                    roleName: 'Lãnh đạo phòng',
                    deptName: 'Văn phòng Cục - THTK',
                    deptId: 62,
                    nodeType: 0,
                    children: [],
                  },
                ],
              },
              {
                check: false,
                id: 40,
                key: '40',
                userId: 15,
                label: 'Trung tâm Chuyển giao công nghệ và Hỗ trợ kỹ thuật',
                code: 'CGHT',
                orgCode: 'THTK',
                activityId: 3,
                isWorkqueue: false,
                parentid: 9,
                eId: 15,
                roleName: 'Lãnh đạo phòng',
                deptName: 'Trung tâm Chuyển giao công nghệ và Hỗ trợ kỹ thuật',
                deptId: 68,
                nodeType: 2,
                children: [
                  {
                    check: false,
                    id: 41,
                    key: '41',
                    userId: 1034,
                    label: 'Nguyễn Ngọc Dũng',
                    code: 'CGHT',
                    orgCode: 'THTK',
                    activityId: 3,
                    isWorkqueue: false,
                    parentid: 10,
                    eId: 14,
                    roleName: 'Lãnh đạo trung tâm',
                    deptName:
                      'Trung tâm Chuyển giao công nghệ và Hỗ trợ kỹ thuật',
                    deptId: 68,
                    nodeType: 0,
                    children: [],
                  },
                  {
                    check: false,
                    id: 42,
                    key: '42',
                    userId: 1031,
                    label: 'Nguyễn Lê Ngọc',
                    code: 'CGHT',
                    orgCode: 'THTK',
                    activityId: 3,
                    isWorkqueue: false,
                    parentid: 10,
                    eId: 15,
                    roleName: 'Phó lãnh đạo trung tâm',
                    deptName:
                      'Trung tâm Chuyển giao công nghệ và Hỗ trợ kỹ thuật',
                    deptId: 68,
                    nodeType: 0,
                    children: [],
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
  },
];
