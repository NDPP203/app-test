import React, { useEffect, useState } from "react";
import AsyncStorage from '@react-native-async-storage/async-storage';
import remove from '../asset/remove.png'
import {
  View,
  Text,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Image,
  StyleSheet,
} from 'react-native';
const pickSchools = () => {
  const [datas, setDatas] = useState([]);
  const [txtSearching, setTxtSearching] = useState('');
  const [searchItems, setSearchItems] = useState(datas);
  const [showLits, setShowList] = useState(false);
  const [schools, setSchool] = useState('Ten truong');
  useEffect(() => {
    Token();
  }, []);
  // const url = 'http://apihrm.vietlott.vn/api/v1/category/schools';
  const Token = async () => {
    let Tokens = await AsyncStorage.getItem('token');

    getData(Tokens);
  }
  const getData = async token => {
    console.log('token', token);
    try {
      const response = await fetch(
        'http://apihrm.vietlott.vn/api/v1/category/schools',
        {
          method: 'GET',
          headers: new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
          }),

        },
      );
      const json = await response.json();
      setDatas(json);
      console.log('-=====', json)
      // const newArr = [...new Set(json)];
      // setDatas(newArr);
    }
    catch {

    }
  }
  const onChangeText = txt => {
    setTxtSearching(txt);
    setShowList(true);
    let filter = datas.filter(function (item) {
      return item.ten.includes(txt);
    });
    if (txt.length > 0 && filter == []) {
      messageText = 'Không tìm thấy giá trị phù hợp';
    }
    setSearchItems(filter);
  };
  const pickSchools = item => {
    setSchool(item.ten);
    setShowList(false);
    setTxtSearching('');
  }
  const removeSchools = () =>{
    setSchool('Ten truong');
  }

  return (
    <View >
      <View style={styles.ViewOne}>
      <Text style={{flex: 1}}>{'Tên trường'}</Text>
      <View style={styles.search}>
      <TextInput
        onChangeText={(txt) => onChangeText(txt)} value={txtSearching}
        placeholder={schools}
        placeholderTextColor='black'
        style={{flex: 1}}
      ></TextInput>
      <TouchableOpacity
      onPress={() => {removeSchools()}}
      >
      <Image source={remove} style={{width: 20, height:20 }} />
      </TouchableOpacity>
      </View>
      </View>
      {showLits ? (
        <ScrollView style={styles.scrollVi}>
          {(txtSearching ? searchItems : datas).map((item, index) => (
            <View>
              <TouchableOpacity
                onPress={() => { pickSchools(item) }}
              >
                <Text style={{ color: 'black' }}>{item.ten}</Text>
              </TouchableOpacity>
            </View>
          ))}
        </ScrollView>
      ) : (
        <></>)}
    </View>
  )

};
const styles = StyleSheet.create({
  ViewOne: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
     alignSelf: 'center',
  },
  search:{
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: 'red',
    height: 40,
    borderRadius: 5,
    margin: 10,
    width: '55%',
    height: 40,
    alignSelf: 'center',
  },
  scrollVi:{
    borderWidth: 1,
    borderColor: 'white',
    height: 40,
    borderRadius: 5,
    margin: 10,
    width: '55%',
    height: '60%',
  }
});
export default pickSchools;
