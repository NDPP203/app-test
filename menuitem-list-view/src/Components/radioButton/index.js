import React, {useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  TouchableOpacity,
  Picker,
  TextInput,
  Button,
} from 'react-native';

const RadioButton = (props) => {

    const statusArray = [
        {id: 1, label: 'Chủ trì', check: false},
        {
          id: 2,
          label: 'Phối hợp',
          check: false,
        },
        {
          id: 3,
          label: 'Thông báo',
          check: false,
        },
      ];

      
      const renderStatus = () => {
        console.log("Call RadioBNT ")

      let obj = props.item 
      console.log("Props.item: ", obj)
    return statusArray.map(item1 => {
      return (
        <TouchableOpacity onPress={() => selectStatus()}>
        <View style={{flexDirection: 'row'}}>
          <Image
            style={{height: 20, width: 20}}
            source={
              props.selectedIds.indexOf(obj) >= 0
                ? require('../../asset/radio-button.png')
                : require('../../asset/radio-button-select.png')
            }
          />
          <Text>{item1.label}</Text>
        </View>
        </TouchableOpacity>
      );
    });
  };


  return (
      <View>

          <Text>{"CALL BNT"}</Text>
          {renderStatus()}
      </View>
  )

      

}
export default RadioButton