import { useNavigation, useRoute } from '@react-navigation/native';
import React, {useEffect} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import {SvgUri} from 'react-native-svg';

const detail = props => {
  const routes = useRoute();
  const dataTd = props.route.params.item;
  useEffect(() => {
    console.log('ITEN IN DETAIL: ', dataTd);
  }, []);
  
  const navigation = useNavigation();
  const goBackApis = () => {
      
      routes.params.getGoBack(dataTd);
      navigation.goBack();
    }
  return (
    <View>
    <TouchableOpacity
    onPress={() => goBackApis()}
    >
      {dataTd.img.slice(-3) == 'svg' ? (
        <SvgUri width="100%" height="100" uri={dataTd.img} />
      ) : (
        <View>
          <Image 
          style={{width: '100%', height: 170}}
          source={{uri: dataTd.img}} />
        </View>
      )}
      <Text>{dataTd.label}</Text>
      <Text>{dataTd.sub_title}</Text>
      <Text>Ngay:{dataTd.date}</Text>
      </TouchableOpacity>
    </View>
  );
};
export default detail;
