import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  FlatList,
  Item,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {SvgUri} from 'react-native-svg';

const ListView = () => {
  const navigation = useNavigation();
  const [fresh, setFresh] = useState(false);
  const goDetails = item => {
    navigation.navigate('Details', {item: item, getGoBack});
  };
  const getGoBack = (item) => {
      data.push(item);
      setFresh(!fresh);
      console.log('data--------', data);
  }

 
  const url = 'http://apihrm.vietlott.vn/api/v1/staff/news';
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [txtSearching, setTxtSearching] = useState('');
  const [pick, setPick] = useState('');
  const [searchItems, setSearchItems] = useState(data);
  // const navigation = useNavigation();
  // const goDetails = () => {
  //     navigation.navigate("Details",)
  // };
useEffect(() => {
    getData();
  }, []);
  const renderItems = ({item}) => <Item title={item.title} />;
  const getData = async () => {
    try {
      const response = await fetch(url);
      const json = await response.json();
      console.log(json);
      const newArr = [...new Set(json)];
      setData(newArr);
      console.log(newArr);
    } catch (error) {
      setLoading(false);
      // console.log(error);
    } finally {
      setLoading(false);
    }
    // const goDetails = () => {
    //     navigation.navigate("Details",)
    // }
  };
  const getitem = (item) => {
    
    console.log('item--------', item);
}

  const onChangeText = txt => {
    setTxtSearching(txt);

    let filter = data.filter(function (item) {
      return item.label.includes(txt);
    });
    if (txt.length > 0 && filter == []) {
      messageText = 'Không tìm thấy giá trị phù hợp';
    }
    setSearchItems(filter);
  };
  console.log('vbnccccc', pick);
  return (
    <ScrollView>
      <View>
      <TextInput onChangeText={onChangeText} value={txtSearching} />
      </View>
       
      { (txtSearching ? searchItems : data).map((item, index) => (
        <View>
          <TouchableOpacity onPress={()=> {getitem(item)}}>
            <Text>{item.id}</Text>
            <Text>{item.label}</Text>
            <Image
              style={{height: 170, width: '100%'}}
              source={{uri: item.img}}
              />
          </TouchableOpacity>
        </View>
      ))}
    </ScrollView>
  );
};
export default ListView;
