import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  TextInput,
  DatePicker,
  Button,
  Image,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {
  launchCamera,
  launchImageLibrary,
  ImagePicker,
} from 'react-native-image-picker';

const Add = props => {
  const dataPassing = props.route.params.item;
  const [label, setLabel] = useState('');
  const [img, setImg] = useState('');
  const [subTitle, setSubTitle] = useState('');
  const [date, getDate] = useState('');
  const navigation = useNavigation();

  const handleChoosePhoto = () => {
    ImagePicker.launchImageLibrary(options, response => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
        alert('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
        alert('ImagePicker Error: ' + response.error);
      } else {
        let source = response;
        this.setState({
          filePath: source,
        });
      }
    });
  };

  const item = 'III';
  const getNewItem = item => {
    let arr = {label: label, img: img, sub_title: subTitle, date: date};
    console.log('item in getNewItem: ', arr);
    props.getNewItem && props.getNewItem(item);
    navigation.goBack();
  };

  return (
    <View>
      <Text>{'Label'}</Text>
      <TextInput
        value={label}
        onChangeText={text => setLabel(text)}
        style={styles.input}
      />

      <Button
        style={styles.input}
        title="Choose Photo"
        onPress={handleChoosePhoto}
      />
      <Text>{'Sub_title'}</Text>
      <TextInput
        value={subTitle}
        onChangeText={text => setSubTitle(text)}
        style={styles.input}
      />

      <Text>{'Date'}</Text>
      <TextInput
        value={date}
        onChangeText={text => getDate(text)}
        style={styles.input}
      />

      <TouchableOpacity
        onPress={() => {
          getNewItem(item);
        }}>
        <Image
          style={{backgroundColor: 'green', width: 50, height: 50, margin: 16}}
          source={require('../asset/edit.png')}
        />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    width: 300,
    height: 40,
    borderRadius: 5,
    backgroundColor: 'red',
  },
});
export default Add;
