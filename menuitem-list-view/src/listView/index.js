import React, {useState, useEffect} from 'react';
import { useNavigation } from '@react-navigation/native';
import {SvgUri} from 'react-native-svg';
import Swipeout from 'react-native-swipeout';
import {
  View,
  Text,
  Image,
  Animated,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  FlatList,
  ActivityIndicator,
} from 'react-native';
const messageText = null;
const ListView = () => {
  const [txtSearch, setTxtSearch] = useState('');
  const [data, setData] = useState([]);

  const [searchItem, setSearchItem] = useState(data);
  const [fresh, setFresh] = useState(false);

  const [loading, setLoading] = useState(true);
  const navigation = useNavigation()
  const url = 'http://apihrm.vietlott.vn/api/v1/staff/news';

  useEffect(() => {
    getData();
  }, []);
  
  const getData = async () => {
    try {
      const response = await fetch(url);
      const json = await response.json();

      const newArr = [...new Set(json)];
      setData(newArr);
      console.log(newArr);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      console.error(error);
    } finally {
      setLoading(false);
    }
  };

  const onChangeText = txt => {
    setTxtSearch(txt);

    let filter = data.filter(function (item) {
      return item.label.includes(txt);
    });
    if (txt.length > 0 && filter == []) {
      messageText = 'Không tìm thấy giá trị phù hợp';
    }
    setSearchItem(filter);
  };
  const deletData = (item)=>{
    if (data.indexOf(item)>=0){
      data.splice(data.indexOf(item),1);
    }
    setFresh(!fresh);
    console.log('data',data)
  }

  const goToDetail = (item) => {
    navigation.navigate("Detail", {item: item})
  } 
  const goToCreate = (item) => {
    navigation.navigate("Add", {item, getNewItem: getNewItem()})
  } 
  const getNewItem = (item) => {
    console.log("Item in lisst view: ", item)
  } 
  
  const renderItem = (item,navigation ) => {
    const swipeSeting = {
      autoClose:true,
      right:[{
        onPress:() => deletData(item),
        text:'delete',
        type: 'delete'
      },{
        onPress: () => goToCreate(item),
        text:'add',
        type:'add',
        backgroundColor:'#2D9D4D'
      },
      {
        onPress:()=>deletData(),
        text:'edit',
        type:'edit',
      }]
    }

    return (
      <Swipeout {...swipeSeting}
      >
        <TouchableOpacity 
        onPress={() => goToDetail(item)}
        style={styles.listItem}>
          {item.img.slice(-3) == 'svg' ? (
            <SvgUri width="40%" height="100" uri={item.img} />
          ) : (
            <View>
              <Image style={styles.image} source={{uri: item.img}}></Image>
            </View>
          )}
          <View style={styles.textItem}>
            <Text>{item.label}</Text>
          </View>
        </TouchableOpacity>
      </Swipeout>
    );
  };
  const renderEmptyContainer = () => {
    return <Text>Khong tim thay gia tri</Text>;
  };
  return (
    <View style={{marginBottom: 100}}>
      <TextInput onChangeText={onChangeText} value={txtSearch} />
      {!loading ? (
        <View
        // contentInsetAdjustmentBehavior="automatic"
        // style={backgroundStyle}
        >
          <FlatList
            data={txtSearch ? searchItem : data}
            renderItem={({item, index}) => renderItem(item, index)}
            keyExtractor={(item, index) => (item.id || index).toString()}
            ListEmptyComponent={renderEmptyContainer()}
          />
        </View>
      ) : (
        <View style={styles.activityIndicatorContainer}>
          <ActivityIndicator style={styles.activityIndicator} />
        </View>
      )}
     
    </View>
  );
};
const styles = StyleSheet.create({
  backgroudsvg: {
    backgroundColor: 'red',
  },
  input:{
    width :200,
    height:50,
    backgroundColor: 'gray'
  }
  ,
  listItem: {marginLeft: 5, flex: 1, flexDirection: 'row', flexWrap: 'nowrap'},
  image: {
    width: 150,
    height: 100,
    borderRadius: 5,
    marginBottom: 8,
  },
  textItem: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginLeft: 5,
  },
  red: {
    color: 'red',
  },
  activityIndicatorContainer: {
    flex: 1,
  },
  activityIndicator: {
    alignContent: 'center',
    justifyContent: 'center',
    flex: 1,
  },
});
export default ListView;
