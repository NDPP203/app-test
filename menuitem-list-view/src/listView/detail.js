import React, {useEffect} from 'react';
import {View, Text,  StyleSheet,Image} from 'react-native';
import {SvgUri} from 'react-native-svg';

const Detail = props => {
  const dataPassing = props.route.params.item;
  useEffect(() => {
    console.log('ITEN IN DETAIL: ', dataPassing);
  }, []);
  
   return (
    <View>
      {dataPassing.img.slice(-3) == 'svg' ? (
        <SvgUri width="100%" height="100" uri={dataPassing.img} />
      ) : (
        <View>
          <Image style={styles.image} source={{uri: dataPassing.img}}></Image>
        </View>
      )}
      <Text>{dataPassing.label}</Text>
      <Text>{dataPassing.sub_title}</Text>
      <Text>Ngay thang {dataPassing.date}</Text>

    </View>

  );
};

const styles = StyleSheet.create({
  image: {
    height: 100,
    borderRadius: 5,
    marginBottom: 8,
  }
});

export default Detail;
