import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Input,
  TextInput,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
  ActivityIndicator,
} from 'react-native';
import {NavigationContainer, useNavigation} from '@react-navigation/native';
import views from '../asset/view.png';
import hide from '../asset/hide.png';
import AsyncStorage from '@react-native-async-storage/async-storage';
const loginn = () => {
  const navigation = useNavigation();
  const goToHeli = () => {
    navigation.navigate('ListView');
  };
  const [data, setData] = useState('');

  useEffect(() => {
    checkToken();
  }, []);

  const checkToken = async () => {
    let value = await AsyncStorage.getItem('token');
    if (value) {
      // await  navigation.navigate('Apis', null);
    }
    await console.log('token', value);
  };
  const getData = async () => {
    setLoading(true);
    try {
      await fetch('http://apihrm.vietlott.vn/api/v1/oauth/sign-in', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          MA: User,
          PAS: Pass,
        }),
      })
        .then(res => {
          if (res.status == 200) {
            console.log('res', res);
            navigation.navigate('HelloDemo', null);
          }
          return res;
        })
        .then(res => res.json())
        .then(dataApi => {
          console.log('RRRRTTT', dataApi)
          if (
            (dataApi.message && dataApi.message == 'Account không tồn tại.') ||
            dataApi.message == 'Bad credentials'
          ) {
            alert(dataApi.message)
            // setMessage({error:dataApi.message,user:" ",password:" "});
            return dataApi;
          }
          
          else if (dataApi.token) {
            AsyncStorage.setItem('token', dataApi.token);
            return dataApi.token;
          }
        });
      setLoading(false);
    } catch (error) {
      console.log('error');
      setLoading(false);
    }
  };
  const [isSecurity, setIsSecurity] = useState(true);
  const [User, setUser] = useState('');
  const [Pass, setPass] = useState('');
  const [loading, setLoading] = useState(false);
  console.log('_________', User);
  console.log('----------', Pass);
  return (
    <View style={{flex: 1}}>
      <ScrollView style={styles.view}>
        <Text style={styles.text}>Vietlott HRM</Text>

        <View>
          <Text style={styles.textv}>User</Text>
          <TextInput
            value={User}
            onChangeText={txt => setUser(txt)}
            style={styles.TextInput}></TextInput>
        </View>
        <Text style={styles.textv}>Password</Text>
        <View style={styles.viewEye}>
          <TextInput
            value={Pass}
            onChangeText={txt => setPass(txt)}
            secureTextEntry={isSecurity}
            style={{flex: 1}}></TextInput>
          <TouchableOpacity
            // style={styles.}
            onPress={() => {
              setIsSecurity(prev => !prev);
            }}>
            {isSecurity ? (
              <Image style={styles.img} source={views} />
            ) : (
              <Image style={styles.img} source={hide} />
            )}
          </TouchableOpacity>
        </View>

        <View>
          <TouchableOpacity
            onPress={() => {
              getData();
            }}
            style={styles.touch}>
            <Text style={styles.texttou}>{'LOGIN'}</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>

      {loading ? (
        <View style={styles.loading}>
          <ActivityIndicator size="large" color="#00ff00" />
        </View>
      ) : (
        <></>
      )}
    </View>
  );
};
const styles = StyleSheet.create({
  view: {
    backgroundColor: '#333333',
  },
  viewEye: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: 'red',
    height: 40,
    borderRadius: 5,
    margin: 10,
    width: '90%',
    height: 40,
    alignSelf: 'center',
  },
  img: {
    padding: 10,
    margin: 5,
    height: 10,
    width: 10,
    resizeMode: 'stretch',
    alignItems: 'center',
  },
  TextInput: {
    width: '90%',
    height: 40,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'red',
    backgroundColor: 'white',
    alignSelf: 'center',
    margin: 14,
  },
  text: {
    color: 'red',
    fontSize: 20,
    textAlign: 'center',
  },
  touch: {
    width: '90%',
    height: 40,
    borderRadius: 5,
    alignSelf: 'center',
    backgroundColor: 'red',
  },
  texttou: {
    textAlign: 'center',
    color: 'white',
    fontSize: 17,
    margin: 10,
  },
  textv: {
    color: 'red',
    marginLeft: 20,
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(175, 175, 175, 0.7)',
  },
});

export default loginn;
